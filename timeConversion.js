const timeConversion = (s = '') => {
    const charPmAm = s[8];
    let out = '';
    const firstChars = Number(s.substring(0,2))
    const restChars = s.substring(2,8);

    if(charPmAm === 'A') {
        if (firstChars === 12) {
            out = '00' + restChars
        } else {
            out = '0' + firstChars + restChars
        }
    } else {
        if (firstChars === 12) {
            out = firstChars + restChars
        } else {
            out = (firstChars + 12) + restChars
        }
    }
    return out;
}

timeConversion('06:40:03AM') // 19:05:45
const { EventEmitter } = require('events');

class TestClass {
    constructor(a = '', b = 0){
        this.a = a;
        this.b = b;
    }

    sum() {
        return this.b + 10;
    }

    addStr() {
        return this.a + ' Added Str'
    }

}

class TestEmitter extends EventEmitter {
    constructor() {
        super()
        this.testClass = new TestClass('Alguna weá', 11);
    }

    doSomething() {
        this.emit('START_PROCESS', 'Process started, data launch')
        if (this.testClass.b > 10) {
            this.emit('SUCCESS', { message: 'Success addition!: ' + this.testClass.sum() })
        } else {
            this.emit('ERROR', { error: 'Number is below 10' })
        }
    }
}

const testEmit = new TestEmitter()

testEmit.on('START_PROCESS', (arg) => console.log(arg))
testEmit.on('SUCCESS', (arg) => console.log(arg.message))
testEmit.on('ERROR', (arg) => console.error(arg.error))

testEmit.doSomething();
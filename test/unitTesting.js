const assert = require('assert');

const sum = (x, y) => x + y;
const substract = (x, y) => x - y;
const multiply = (x, y) => x * y;
const divide = (x, y) => x / y;




const mathTesting = () => {
    // Sum
    it('Should success sum the numbers',() => {
        assert.equal(sum(2,2), 4);
    });
    //Substract
    it('Should substract the numbers',() => {
        assert.equal(substract(2,2), 0);
    });
    // Multiply
    it('Should multiply the numbers',() => {
        assert.equal(multiply(3,3), 9);
    });

    // Divide
    it('Should divide the numbers',() => {
        assert.equal(divide(9,3), 3);
    });
}

describe('It should test mathematical operations', mathTesting)
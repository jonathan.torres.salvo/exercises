const https = require('node:https');

const httpHandler = () => new Promise((resolve, reject) => {
    let chunks = '';
    https.get('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1', (res) => {
    console.log('statusCode:', res.statusCode);
    console.log('headers:', res.headers);

    res.on('data', (d) => { 
        chunks += d; 
    });
    res.on('end', () => { 
        try {
            const data = JSON.parse(chunks);
            resolve({ headers: {...res.headers, status: res.statusCode}, data });
          } catch (e) {
            reject(e.message);
        }
     });

    }).on('error', (e) => {
        console.error(e);
    });
})
const test = async () => {
    try {
        const res = await httpHandler();
        console.log(res);
    } catch (error) {
        console.log(error);
    }
}
test();
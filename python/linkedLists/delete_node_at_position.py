# https://www.hackerrank.com/challenges/delete-a-node-from-a-linked-list/problem?isFullScreen=true&h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
from LinkedList import SinglyLinkedList
def delete_node_at_position(head: Sing, position):
    temp = head
    prev = head
    for i in range(0, position):
        if i == 0 and position == 1:
            head = head.next
 
        else:
            # This conditional makes the link between the previous node of the node to be deleted and the next of it.
            if i == position-1 and temp is not None:
                prev.next = temp.next
            else:
                prev = temp
 
                # Position was greater than
                # number of nodes in the list
                if prev is None:
                    break
                # Go to the node to be deleted
                temp = temp.next
    return head


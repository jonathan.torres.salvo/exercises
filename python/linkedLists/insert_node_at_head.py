# https://www.hackerrank.com/challenges/insert-a-node-at-the-head-of-a-linked-list/
class SinglyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None

class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

def insert_node_at_head(head, data):
    new_node = SinglyLinkedListNode(data)
    new_node.next = head
    head = new_node
    return head
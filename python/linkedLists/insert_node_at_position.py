# https://www.hackerrank.com/challenges/insert-a-node-at-a-specific-position-in-a-linked-list/problem?isFullScreen=true&h_r=next-challenge&h_v=zen
import LinkedList
def insert_node_at_position(head, data, position):
    new_node = LinkedList.SinglyLinkedListNode(data)
    last_position = head
    for i in range(position - 1):
        last_position = last_position.next
    new_node.next = last_position.next
    last_position.next = new_node
    return head
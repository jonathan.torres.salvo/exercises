import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LoggerModule } from 'nestjs-pino';
import { CardsModule } from './cards/cards.module';

@Module({
  imports: [
    CardsModule,
    LoggerModule.forRoot({ pinoHttp: { autoLogging: false }}),
    MongooseModule.forRoot('mongodb')
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

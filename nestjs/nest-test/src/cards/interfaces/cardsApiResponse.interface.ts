import { Card } from "./card.interface";

export interface CardApiResponse {
    success: boolean,
    cards: Card[],
    deck_id: string,
    remaining: number,
}
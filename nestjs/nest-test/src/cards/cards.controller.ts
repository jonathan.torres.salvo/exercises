import { Controller, Get, Query } from '@nestjs/common';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { CardsService } from './cards.service';
import { Deck } from './interfaces/deck.interface';

@Controller('cards')
export class CardsController {
  constructor(
    private readonly cardsService: CardsService,
    @InjectPinoLogger(CardsController.name) private readonly logger: PinoLogger
  ) {}

  @Get()
   async getCards(): Promise<Deck> {
    const resp = await this.cardsService.getDeck();
    this.logger.info({ resp }, `Got cards succesfully!`);
    return resp;
  }
}

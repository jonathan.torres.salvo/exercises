import { HttpService } from '@nestjs/axios';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { lastValueFrom, map } from 'rxjs';
import { Deck } from './interfaces/deck.interface';

@Injectable()
export class CardsService implements OnModuleInit {
  constructor(private readonly httpService: HttpService) {}

  onModuleInit() {
    
  }
  
  getDeck(): Promise<Deck> {
    const deck = this.httpService.get(`https://deckofcardsapi.com/api/deck/new/draw/?count=52`).pipe(
      map(resp => resp.data)
    );
    return lastValueFrom(deck);
  }
}

/* When To Use A Factory Pattern

    When our object creation process involves a high level of complexity
    When we need to return different instances of an object depending on some dynamic factors or application configuration
    When a class can’t determine the subclass it must create
    When we need to create different small objects that share some properties

Pros of the factory pattern

    Promotes loose decoupling by separating the object creation from its implementation
    Enables us to create different instances based on conditions
    Promotes code reusability and maintainability
    Encapsulates the constructor or class and exposes only a defined interface.

Cons of the factory pattern

    Can be complex to implement in some cases
    Depending on the complexity can be difficult to test because of the level of abstraction it introduces. */

function Vehicle (options) {
    this.wheels = options.wheels || 4;
    this.doors = options.doors || 4;
    this.color = options.color || "white"; 
}

class Truck extends Vehicle {
    constructor(options) {
        super(options);
        this.truckSpecialField = 'This is a truck';
    }

    liftCharger () {
        console.log('Lifting truck charger');
    }
}

class Motorcycle extends Vehicle {
    constructor(options) {
        super(options);
        this.motoSpecialField = 'This is a moto';
    }

    goOn1Wheel() {
        console.log('Motorcycle on one wheel');
    }
}

class Car extends Vehicle {
    constructor(options) {
        super(options);
        this.carSpecialField = 'This is a car';
    }

    doSomething() {
        console.log('Car is doing something');
    }

}

class VehicleFactory {

    create = (options, type) => {
        const creationDictionary = {
            'car': new Car(options),
            'truck': new Truck(options),
            'moto': new Motorcycle(options),
        }
        if(!type) {
            throw 'Must have type parameter'
        }

        return creationDictionary[type];
    }
}
const factory = new VehicleFactory();
const car1 = factory.create({ wheels: 4, doors: 6, color: 'negro' }, 'car');
const car2 = factory.create({ wheels: 4, doors: 4, color: 'blanco' }, 'car');
const truck = factory.create({ wheels: 8, doors: 3, color: 'gris' }, 'truck');
const moto = factory.create({ wheels: 2, doors: 8, color: 'rojo' }, 'moto');

car1.doSomething();
moto.goOn1Wheel();
truck.liftCharger();

console.log(car1);
console.log(car2);
console.log(truck);
console.log(moto);
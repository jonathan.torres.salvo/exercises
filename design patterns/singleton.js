class someClass {
    constructor(description) {
        this.description = description;
    }
}
class singletonClass {
    instance;
    constructor(desc) {
        this.createInstance(desc);
    }
    createInstance = (desc) => {
        if (!this.instance) {
            this.instance = new someClass(desc);
        }
        console.log(this.instance.description)
    }
}
// Here the old instance is maintained on used instead of create a new one.
const x = new singletonClass('Old instance');
x.createInstance('New instance');
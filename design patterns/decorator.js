class SomeClass {
    constructor() {
        this.value = 10
        this.description = "Description"
    }
}
/**
 * @const {someClass} someClass 
 */
class SomeHigherClass {
    constructor(someClass) {
        this.class = someClass || new SomeClass();
    }
    getHigherValue() {
        return this.class.value + 10;
    }
    getMoreDescription() {
        return this.class.description + ': bla bla';
    }
}
const someHigherClass = new SomeHigherClass();
console.log(someHigherClass.getHigherValue());
console.log(someHigherClass.getMoreDescription());
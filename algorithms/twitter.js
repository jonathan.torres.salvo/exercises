//https://leetcode.com/problems/design-twitter/
/**
 * Runtime: 83 ms, faster than 46.83% of JavaScript online submissions for Design Twitter.
 * Memory Usage: 42.4 MB, less than 23.41% of JavaScript online submissions for Design Twitter.
 */
class Twitter {
    constructor() {
        this.users = [];
        this.tweets = [];
    }

    createUser = (userId) => {
        this.users[userId] = []
    }

    postTweet = (userId, tweetId) => {
        if(!this.users[userId]) this.createUser(userId);
        this.tweets.unshift([userId, tweetId]);
    };

    getNewsFeed = (userId) => {
        let out = [];
        const follows = this.users[userId];
        for (let i = 0; i < this.tweets.length; i++) {
            const [ user, tweetId ] = this.tweets[i];
            if(follows.includes(user) || user === userId) {
                out.push(tweetId)
            }
            if(out.length >= 10) break;
        }
        return out;
    };

    follow = (followerId, followeeId) => {
        if(!this.users[followerId]) this.createUser(followerId);
        if(!this.users[followeeId]) this.createUser(followeeId);
        this.users[followerId].push(followeeId);
    };

    unfollow = (followerId, followeeId) => {
        for (let i = 0; i < this.users[followerId].length; i++) {
            if (this.users[followerId][i] === followeeId) return this.users[followerId].splice(i, 1);
        }
    };
}
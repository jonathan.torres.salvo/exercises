const validPath = (n, edges, source, destination) => {
    let out = false;
    // Defines the graphs and its adjacency
    const graph = new Array(n);
    for (let i = 0; i < edges.length; i++) {
        const [inI, val] = edges[i];
        if (graph[inI] === undefined) {
            graph[inI] = [val]
        } else {
            graph[inI].push(val)
        }
        if (graph[val] === undefined) {
            graph[val] = [inI]
        } else {
            graph[val].push(inI)
        }
    }
    // Traverses the graph
    const traverse = (node, visited = {}, graph = [], dest, out) => {
        if (node === dest) out = true;
        const graphNode = graph[node];
        visited[node] = true;
        console.log(node)
        if (Array.isArray(graphNode) && graphNode.length > 0)
            for (let i = 0; i < graphNode.length; i++)
                if (visited[graphNode[i]] !== true) out = traverse(graphNode[i], visited, graph, dest, out);
        return out
    }
    out = traverse(source, {}, graph, destination, out)
    return out
};
console.log(validPath(10, [[4,3],[1,4],[4,8],[1,7],[6,4],[4,2],[7,4],[4,0],[0,9],[5,4]], 5, 9))
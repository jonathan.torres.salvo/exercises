//https://www.hackerrank.com/challenges/encryption/problem
function encryption(s) {
    let c,r, out='';
    let rows = [];
    const cadena = s.replace(/\s/g,'').split("");
    c = Math.ceil(Math.sqrt(cadena.length));
    r = Math.floor(Math.sqrt(cadena.length));
    if (r * c < cadena.length) r=c;
    
    let start = 0;
    for (let index = 0; index < r; index++) {
        const cadenaRecortada = cadena.splice(start, c);
        rows.push(cadenaRecortada);
    }
    for (let cIndex = 0; cIndex < c; cIndex++) {
        for (let rIndex = 0; rIndex < r; rIndex++) {
            if (rows[rIndex][cIndex] !== undefined){
                out += rows[rIndex][cIndex];
            }        
        };
        out += ' ';  
    }    
    return out;
}

console.log(encryption('chillout'))
/**
 * @param {number[][]} graph
 * @return {number[][]}
 * DFS algorithm to traverse through paths
 */
const allPathsSourceTarget = function(graph) {
    const recStack = [];
    const traverse = (node, path = [], graph = []) =>
    {
        path.push(node)
        if(node === graph.length - 1)
        {
            recStack.push([...path]);
            path.pop()
            return 
        }

        for(let i of graph[node])
            traverse(i, path, graph);
        path.pop()
    }

    traverse(0, [], graph)
    return recStack
};

console.log(allPathsSourceTarget( [[1,2],[3],[3],[]] ))
console.log(allPathsSourceTarget( [[4,3,1],[3,2,4],[3],[4],[]] ))
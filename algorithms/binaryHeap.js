class MaxHeap {
    constructor(maxSize = 10) {
        this.heap = new Array(maxSize);
        this.size = 0;
        this.maxSize = maxSize
    }

    getParent(pos = 0) {
        return Number.parseInt(((pos - 1) / 2))
    }

    getLeftChild(pos = 0) {
        return (2 * pos) + 1
    }

    getRightChild(pos = 0) {
        return (2 * pos) +2
    }

    isLeaf(pos = 0) {
        if(pos > (this.size / 2) && pos <= this.size) return true;
        return false;
    }

    swap(fpos = 0, spos = 0) {
        let tmp;
        tmp = this.heap[fpos];
        this.heap[fpos] = this.heap[spos];
        this.heap[spos] = tmp;
    }

    maxHeapify(pos = 0) {
        const leftChild = this.getLeftChild(pos);
        const rightChild = this.getRightChild(pos);

        if (this.isLeaf(pos)) return;

        if(this.heap[pos] < this.heap[leftChild] || this.heap[pos] < this.heap[rightChild]) {
            if (this.heap[leftChild] > this.heap[rightChild]) {
                this.swap(pos, leftChild);
                this.maxHeapify(leftChild);
            } else {
                this.swap(pos,rightChild);
                this.maxHeapify(rightChild)
            }
        }
    }

    insert(element = 0) {
        let current = this.size
        this.heap[current] = element;
        this.size++;

        while(this.heap[current] > this.heap[this.getParent(current)]) {
            this.swap(current, this.getParent(current))
            current = this.getParent(current);
        }
    }

    print() {
        for (let i = 0; i < this.size; i++) {
            const element = this.heap[i];
            console.log('---------------------------');
            console.log(`Parent: ${element}`);

            if(this.getLeftChild(i) < this.size) console.log(`Left Child: ${this.heap[this.getLeftChild(i)]}`)
            if(this.getRightChild(i) < this.size) console.log(`Right Child: ${this.heap[this.getRightChild(i)]}`)
            console.log('---------------------------');
        }
    }

    extractMax() {
        const popped = this.heap[0];
        this.heap[0] = this.heap[--this.size];
        this.maxHeapify(0);
        return popped
    }
}

maxHeap = new MaxHeap();
 
// Inserting nodes
// Custom inputs
maxHeap.insert(5);
maxHeap.insert(3);
maxHeap.insert(17);
maxHeap.insert(10);
maxHeap.insert(84);
maxHeap.insert(19);
maxHeap.insert(6);
maxHeap.insert(22);
maxHeap.insert(9);

console.log(maxHeap.heap)
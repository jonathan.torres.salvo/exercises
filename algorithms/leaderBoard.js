//Leaderboard exercise implementing binarySearch algorithm
function leaderBoard(ranked = [], player = []) {
    let pos = 0;

    const rankedPositions = ranked.map((rank, i, ar) => {
        if (rank != ar[i - 1]) {
            pos = pos + 1
            return { pos, value: rank }
        }
        return { pos, value: rank }
    })
    function binarySearch(arr, l, r, x) {
        if (r >= l) {
            let mid = l + Math.floor((r - l) / 2);
            if (arr[mid].value === x)
                return arr[mid].pos;
            if (arr[mid + 1] !== undefined)
                if (arr[mid].value > x && x > arr[mid + 1].value)
                    return arr[mid].pos + 1;
            if (arr[mid + 1] === undefined)
                if (arr[mid].value !== x)
                    return arr[mid].pos + 1;
                else
                    return arr[mid].pos;
            if (arr[mid - 1] === undefined)
                if (x >= arr[mid].value)
                    return arr[mid].pos;

            if (arr[mid].value > x) {
                return binarySearch(arr, mid + 1, r, x);
            }
            if (arr[mid].value < x) {
                return binarySearch(arr, l, mid - 1, x);
            }
        }
        return -1;
    }
    const positioned = player.map((playerRank) => binarySearch(rankedPositions, 0, rankedPositions.length - 1, playerRank))
    return positioned
}
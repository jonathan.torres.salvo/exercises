/**
 * Author: Jonathan Torres 20-06-2022
 * 
 * This is simple expressJs test where I setup a couple of routes for a success and an error case doing get requests.
 */

const express = require('express');

class Server {
    constructor() {
        this.app = express();
    }

    // Router test
    useInitialRouter = function() {
        const router = express.Router();
        router.get('/success', (req, res) => { 
            console.log('message received');
            res.status(200).json({ message: 'OK!' });
        })

        router.get('/error', (req, res) => { 
            console.log('message received');
            throw 'An error ocurred!';
        })
        console.log('Setup route')
        return router;
    }

    // Error middleware test
    errorMiddleware (error, req, res, next) {
        console.log(`Error!: ${error}`);
        res.status(500).json({ error });
    }

    init() {
        const port = 3000;
        
        this.app.use(this.useInitialRouter());
        this.app.use(this.errorMiddleware);
        this.app.listen(port, () => {
            console.log(`Server listening on port ${port}`);
        })
    }    
}
const svr = new Server();
svr.init();
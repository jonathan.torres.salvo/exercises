export interface ProductRepository {
    create(product);
    findAll();
    findByType(type: string);
}
import { Controller, Get, Post } from '@nestjs/common';

@Controller('product')
export class ProductController {
    @Get()
    findAll(): string {
        return 'This action returns all products';
    }

    @Get('type')
    findByType(): string {
        return 'This action returns products by type';
    }

    @Post()
    create(): string {
        return 'This action creates a product';
    }
}

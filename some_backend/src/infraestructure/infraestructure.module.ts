import { Module } from '@nestjs/common';
import { ProductController } from './inbound/rest/product.controller';

@Module({
  imports: [],
  controllers: [ProductController],
  providers: [],
})
export class InfraestructureModule { }
